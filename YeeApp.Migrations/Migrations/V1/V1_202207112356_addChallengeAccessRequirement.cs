using FluentMigrator;

namespace YeeApp.Migrations.Migrations.V1; 

[Migration(202207112356, "FANZ-4. Add Challenge Access Requirement")]
public class V1_202207112356_addChallengeAccessRequirement : Migration {
    public override void Up() {
        Create.Table("challengeaccessrequirement")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("challengeId").AsInt32().NotNullable()
            .WithColumn("requirementType").AsString(100).NotNullable().Unique()
            .WithColumn("requirementValue").AsInt32().NotNullable()
            .WithColumn("isDeleted").AsBoolean().NotNullable().WithDefaultValue(false);
        
        Create.UniqueConstraint()
            .OnTable("challengeaccessrequirement")
            .Columns("challengeId", "isDeleted");
        Create.UniqueConstraint()
            .OnTable("challengeaccessrequirement")
            .Columns("requirementType", "requirementValue", "isDeleted");
        Create.UniqueConstraint()
            .OnTable("challengeaccessrequirement")
            .Columns("challengeId", "requirementType", "requirementValue");
    }

    public override void Down() {
        Delete.Table("challengeaccessrequirement");
    }
}