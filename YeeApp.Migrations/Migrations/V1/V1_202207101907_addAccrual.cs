using FluentMigrator;

namespace YeeApp.Migrations.Migrations.V1; 

[Migration(202207101907, "FANZ-2. Add accrual source")]
public class V1_202207101907_addAccrual  : Migration{
    public override void Up() {
        Create.Table("accrualsourcetypeconfig")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("sourceType").AsString(100).NotNullable().Unique()
            .WithColumn("workMode").AsString(100).NotNullable()
            .WithColumn("points").AsFloat().NotNullable();
        
        Create.Table("experiencepointsaccrual")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32().NotNullable().Indexed()
            .WithColumn("sourceType").AsString(100).NotNullable()
            .WithColumn("sourceEntityId").AsInt32().NotNullable()
            .WithColumn("sourceEntity").AsString(100).NotNullable()
            .WithColumn("points").AsFloat().NotNullable()
            .WithColumn("createdAt").AsDateTimeOffset().NotNullable();

        Create.Table("customerlevel")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32().NotNullable().Unique()
            .WithColumn("totalExperiencePoints").AsFloat().NotNullable()
            .WithColumn("currentLevel").AsInt32().NotNullable();
    }

    public override void Down() {
        Delete.Table("accrualsourcetypeconfig");
        Delete.Table("experiencepointsaccrual");
        Delete.Table("customerlevel");
    }
}