using FluentMigrator;

namespace YeeApp.Migrations.Migrations.V1; 

[Migration(202207112313, "FANZ-3. Add preview challenge")]
public class V1_202207112313_addChallengePreview : Migration {
    public override void Up() {
        Create.Table("challengepreview")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("challengeType").AsString(100).NotNullable().Unique()
            .WithColumn("previewUrl").AsString(2000).NotNullable();

        Create.Table("customerchallengepreview")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("userAccountId").AsInt32().Identity().NotNullable()
            .WithColumn("challengeType").AsString(100).NotNullable();
    }

    public override void Down() {
        Delete.Table("challengepreview");
        Delete.Table("customerchallengepreview");
    }
}