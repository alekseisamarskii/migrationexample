using FluentMigrator;

namespace YeeApp.Migrations.Migrations.V1; 

[Migration(202207241213, "FANZ-5. Add quizzes")]
public class V1_202207241213_addQuiz : Migration{
    public override void Up() {
        Create.Table("quizlevel")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("challengeId").AsInt32().NotNullable()
            .WithColumn("nextLevelId").AsInt32().Nullable()
            .WithColumn("title").AsString(100)
            .WithColumn("description").AsString(1000)
            .WithColumn("orderNumber").AsInt16()
            .WithColumn("pointsToUnlock").AsInt32()
            .WithColumn("isDeleted").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("createdAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
            .WithColumn("updatedAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);
        
        Create.Table("quizlevelquestion")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("quizLevelId").AsInt32().NotNullable()
            .WithColumn("body").AsString(1000)
            .WithColumn("correctAnswerHint").AsString(300)
            .WithColumn("incorrectAnswerHint").AsString(300)
            .WithColumn("timelimit").AsInt32().Nullable()
            .WithColumn("isDeleted").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("createdAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
            .WithColumn("updatedAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);
        
        Create.Table("quizcustomerlevel")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32().NotNullable()
            .WithColumn("quizId").AsInt32().NotNullable()
            .WithColumn("quizLevelId").AsInt32().NotNullable()
            .WithColumn("correctAnswersCount").AsInt16()
            .WithColumn("wrongAnswersCount").AsInt16()
            .WithColumn("earnedPoints").AsInt32()
            .WithColumn("startDate").AsDateTimeOffset().Nullable()
            .WithColumn("completionDate").AsDateTimeOffset().Nullable()
            .WithColumn("createdAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
            .WithColumn("updatedAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);
        
        Create.Table("quizcustomerquestion")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32().NotNullable()
            .WithColumn("quizLevelId").AsInt32().NotNullable()
            .WithColumn("quizQuestionId").AsInt32().NotNullable()
            .WithColumn("answerId").AsInt32().Nullable()
            .WithColumn("customAnswer").AsString(1000)
            .WithColumn("questionState").AsString(30)
            .WithColumn("createdAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
            .WithColumn("updatedAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);
        
        Create.Table("quizquestionanswer")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("quizQuestionId").AsInt32().NotNullable()
            .WithColumn("bannerId").AsString(300)
            .WithColumn("body").AsString(1000)
            .WithColumn("answerState").AsString(30)
            .WithColumn("isDeleted").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("createdAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
            .WithColumn("updatedAt").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);
    }

    public override void Down() {
        Delete.Table("quizlevel");
        Delete.Table("quizlevelquestion");
        Delete.Table("quizcustomerlevel");
        Delete.Table("quizcustomerquestion");
        Delete.Table("quizquestionanswer");
    }
}