using FluentMigrator;

namespace YeeApp.Migrations.Migrations.V1;

/*                 migration example             */

/*[Migration(202206031314, "FANZ-999. Create table migr_test for YeeApp")]
public class V1_202206031314_init : Migration {
    public override void Up() {
        Create.Table("migr_test")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("name").AsString(255).NotNullable()
            .WithColumn("created_at").AsDateTimeOffset().NotNullable()
            .WithColumn("updated_at").AsDateTimeOffset().NotNullable();
        // for irregularly index
        // or create index manually use command Execute.Sql() below(just example)
        Execute.Sql("CREATE INDEX migr_test_created_at ON migr_test (created_at)");
        // OR
        Execute.Sql(@"
          CREATE OR REPLACE FUNCTION questions_tsvector (title text, body text)
            RETURNS tsvector
            AS $$
          BEGIN
            RETURN (setweight(to_tsvector('english', title), 'A') || setweight(to_tsvector('english', body), 'B'));
          END
          $$
          LANGUAGE 'plpgsql'
          IMMUTABLE;");

        Execute.Sql(@"
          CREATE INDEX IF NOT EXISTS ix_questions_title_body ON questions
            USING GIN(questions_tsvector(title, body));");
    }

    public override void Down() {
        Delete.Table("migr_test");
    }
}*/