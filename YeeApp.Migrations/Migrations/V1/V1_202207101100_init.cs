using FluentMigrator;
using FluentMigrator.Postgres;

namespace YeeApp.Migrations.Migrations.V1;

[Migration(202206031314, "FANZ-1. Init for YeeApp.Backend")]
public class V1_202207101100_init : Migration {
    public override void Up() {
        Create.Table("challenge")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("publisherId").AsInt32()
            .WithColumn("title").AsString(90).NotNullable()
            .WithColumn("description").AsString(1000).NotNullable()
            .WithColumn("type").AsString(100).NotNullable()
            .WithColumn("taskConfig").AsString(5000)
            .WithColumn("prizeConfig").AsString(2000).NotNullable()
            .WithColumn("pictureUrl").AsString(1000)
            .WithColumn("imageId").AsString(300)
            .WithColumn("creationDate").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime)
            .WithColumn("validTillDate").AsDateTimeOffset().NotNullable()
            .WithColumn("deleted").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("deletionDate").AsDateTimeOffset().Nullable();
        Execute.Sql(@"ALTER TABLE challenge 
                        ADD textsSearchable tsvector GENERATED ALWAYS AS (to_tsvector('english', title || ' ' || description)) STORED;
                      CREATE INDEX challenge_textsSearchable ON challenge USING GIN (textsSearchable)");

        Create.Table("useraccount")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("hashPassword").AsString(1000)
            .WithColumn("phoneNumber").AsString(100).Unique()
            .WithColumn("phoneNumberConfirmed").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("email").AsString(100).Unique()
            .WithColumn("emailConfirmed").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("isExternalUser").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("registrationDate").AsDateTimeOffset().WithDefault(SystemMethods.CurrentUTCDateTime);

        Create.Table("customer")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("userAccountId").AsInt32().NotNullable().Unique()
            .WithColumn("nickname").AsString(100)
            .WithColumn("firstName").AsString(100)
            .WithColumn("middleName").AsString(100)
            .WithColumn("lastName").AsString(100)
            .WithColumn("avatarUrl").AsString(1000)
            .WithColumn("avatarImageId").AsString(300)
            .WithColumn("emailMarketingEnabled").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("bannerUrl").AsString(1000)
            .WithColumn("bannerImageId").AsString(300);

        Create.Table("actionstate")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("title").AsString(100).NotNullable()
            .WithColumn("description").AsString(1000).NotNullable()
            .WithColumn("actionStateType").AsString(100).NotNullable()
            .WithColumn("config").AsString(3000).NotNullable()
            .WithColumn("validAfterDate").AsDateTimeOffset()
            .WithColumn("validTillDate").AsDateTimeOffset()
            .WithColumn("forUnauthorized").AsBoolean().NotNullable().WithDefaultValue(false);

        Create.Table("customeractionstate")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32().NotNullable()
            .WithColumn("actionStateId").AsInt32().NotNullable();
        Create.UniqueConstraint().OnTable("customeractionstate").Columns("customerId", "actionStateId");

        Create.Table("publisher")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("userAccountId").AsInt32().NotNullable().Unique()
            .WithColumn("title").AsString(100)
            .WithColumn("avatarUrl").AsString(1000)
            .WithColumn("avatarImageId").AsString(300)
            .WithColumn("about").AsString(1000)
            .WithColumn("color").AsString(10)
            .WithColumn("headerColor").AsString(10)
            .WithColumn("backgroundColor").AsString(10)
            .WithColumn("hasExclusiveContract").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("activated").AsBoolean().NotNullable().WithDefaultValue(true)
            .WithColumn("bannerUrl").AsString(1000)
            .WithColumn("bannerImageId").AsString(300)
            .WithColumn("gatewayConfiguration").AsString(5000);
        Execute.Sql(@"ALTER TABLE publisher 
                        ADD textsSearchable tsvector GENERATED ALWAYS AS (to_tsvector('english', title || ' ' || coalesce(about, ''))) STORED;
                      CREATE INDEX publisher_textsSearchable ON publisher USING GIN (textsSearchable)");

        Create.Table("collection")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("publisherId").AsInt32()
            .WithColumn("title").AsString(90).NotNullable()
            .WithColumn("description").AsString(1000).NotNullable()
            .WithColumn("contractAddress").AsString(100)
            .WithColumn("imageId").AsString(300)
            .WithColumn("bcType").AsString(100).NotNullable()
            .WithColumn("creationDate").AsDateTimeOffset().WithDefault(SystemMethods.CurrentUTCDateTime);
        
        Create.Table("tokenitem")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("tokenItemTypeId").AsInt32().NotNullable().Indexed()
            .WithColumn("serialNumber").AsInt32().NotNullable()
            .WithColumn("ownerCustomerId").AsInt32().Indexed()
            .WithColumn("contractAddress").AsString(100)
            .WithColumn("status").AsString(100).NotNullable()
            .WithColumn("creationDate").AsDateTimeOffset().WithDefault(SystemMethods.CurrentUTCDateTime);

        Create.Table("tokenitemtype")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("collectionId").AsInt32()
            .WithColumn("tokenId").AsInt32()
            .WithColumn("title").AsString(90).NotNullable()
            .WithColumn("description").AsString(1000).NotNullable()
            .WithColumn("contentUrl").AsString(1000)
            .WithColumn("imageId").AsString(300)
            .WithColumn("mediaType").AsString(100).NotNullable()
            .WithColumn("rarity").AsString(100).NotNullable()
            .WithColumn("totalSupply").AsInt32()
            .WithColumn("count").AsInt32()
            .WithColumn("contractAddress").AsString(300)
            .WithColumn("creationDate").AsDateTimeOffset().WithDefault(SystemMethods.CurrentUTCDateTime);
        Execute.Sql(@"ALTER TABLE tokenitemtype 
                        ADD textsSearchable tsvector GENERATED ALWAYS AS (to_tsvector('english', title || ' ' || description)) STORED;
                      CREATE INDEX tokenitemtype_textsSearchable ON tokenitemtype USING GIN (textsSearchable)");

        Create.Table("category")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("title").AsString(90).NotNullable();
        
        Create.Table("tokenitemtypecategory")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("tokenItemTypeId").AsInt32().NotNullable()
            .WithColumn("categoryId").AsInt32().NotNullable()
            .WithColumn("deleted").AsBoolean().NotNullable().WithDefaultValue(false);
        Create.UniqueConstraint().OnTable("tokenitemtypecategory").Columns("tokenItemTypeId","categoryId");
        
        Create.Table("package")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("title").AsString(90).NotNullable()
            .WithColumn("description").AsString(1000).NotNullable()
            .WithColumn("pictureUrl").AsString(1000)
            .WithColumn("imageId").AsString(300)
            .WithColumn("price").AsInt32()
            .WithColumn("dropConfig").AsString(5000).NotNullable()
            .WithColumn("startDate").AsDateTimeOffset().NotNullable()
            .WithColumn("endDate").AsDateTimeOffset().NotNullable()
            .WithColumn("deleted").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("deletionDate").AsDateTimeOffset().Nullable()
            .WithColumn("creationDate").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);
        
        Create.Table("refreshtoken")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("userAccountId").AsInt32().NotNullable()
            .WithColumn("deviceId").AsString(100)
            .WithColumn("deviceType").AsString(100)
            .WithColumn("token").AsString(44).NotNullable().Indexed()
            .WithColumn("revoked").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("expirationDate").AsDateTimeOffset().NotNullable();
        Create.UniqueConstraint().OnTable("refreshtoken").Columns("userAccountId","revoked","expirationDate");
        
        Create.Table("userrole")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("userAccountId").AsInt32().NotNullable()
            .WithColumn("role").AsString(90)
            .WithColumn("isActive").AsBoolean().NotNullable().WithDefaultValue(false);
        Create.UniqueConstraint().OnTable("userrole").Columns("userAccountId","role");

        Create.Table("customerpackage")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("packageId").AsInt32().NotNullable()
            .WithColumn("customerId").AsInt32().NotNullable().Indexed()
            .WithColumn("status").AsString(100).NotNullable()
            .WithColumn("openingDate").AsDateTimeOffset().Nullable()
            .WithColumn("creationDate").AsDateTimeOffset().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);

        Create.Table("packagecontent")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("packageId").AsInt32().NotNullable().Indexed()
            .WithColumn("contentId").AsInt32().NotNullable()
            .WithColumn("contentType").AsString(100).NotNullable()
            .WithColumn("deleted").AsBoolean().NotNullable().WithDefaultValue(false);
        Create.UniqueConstraint().OnTable("packagecontent").Columns("contentId","contentType");

        Create.Table("droppedhistory")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32()
            .WithColumn("droppedItemId").AsInt32()
            .WithColumn("droppedItemType").AsString(100).NotNullable()
            .WithColumn("sourceId").AsInt32()
            .WithColumn("sourceType").AsString(100).NotNullable()
            .WithColumn("receivingDate").AsDateTimeOffset().WithDefault(SystemMethods.CurrentUTCDateTime);

        Create.Table("customerfollowing")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("destinationId").AsInt32()
            .WithColumn("destinationType").AsString(100).NotNullable()
            .WithColumn("customerId").AsInt32().Indexed()
            .WithColumn("deleted").AsBoolean().NotNullable().WithDefaultValue(false);
        Create.UniqueConstraint().OnTable("customerfollowing").Columns("destinationType","destinationId", "customerId");

        Create.Table("otpverification")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("sid").AsString(100).NotNullable().Indexed()
            .WithColumn("userAccountId").AsInt32().NotNullable()
            .WithColumn("recipient").AsString(100).NotNullable()
            .WithColumn("success").AsBoolean().NotNullable().WithDefaultValue(false)
            .WithColumn("creationDate").AsDateTimeOffset().WithDefault(SystemMethods.CurrentUTCDateTime);

        Create.Table("customerchallenge")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32().NotNullable()
            .WithColumn("challengeId").AsInt32().NotNullable()
            .WithColumn("state").AsString(100).NotNullable()
            .WithColumn("additionalInfo").AsString(5000);
        Create.UniqueConstraint().OnTable("customerchallenge").Columns("customerId","challengeId", "state");
        
        Create.Table("challengecontent")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("challengeId").AsInt32()
            .WithColumn("contentId").AsInt32().Indexed()
            .WithColumn("contentType").AsString(100).NotNullable()
            .WithColumn("deleted").AsBoolean().NotNullable().WithDefaultValue(false);
        
        Create.Table("customerorder")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerId").AsInt32().Indexed()
            .WithColumn("status").AsString(100).NotNullable()
            .WithColumn("amount").AsInt32().NotNullable()
            .WithColumn("creationDate").AsDateTimeOffset().WithDefault(SystemMethods.CurrentUTCDateTime);
        
        Create.Table("customerorderitem")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerOrderId").AsInt32().NotNullable().Indexed()
            .WithColumn("productId").AsInt32().NotNullable()
            .WithColumn("productType").AsString(100).NotNullable()
            .WithColumn("count").AsInt32().NotNullable()
            .WithColumn("price").AsInt32().NotNullable()
            .WithColumn("deleted").AsBoolean().NotNullable().WithDefaultValue(false);

        Create.Table("payment")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerOrderId").AsInt32().NotNullable().Indexed()
            .WithColumn("status").AsString(100).NotNullable()
            .WithColumn("externalId").AsString(100).Unique();
        
        Create.Table("useraccountwallet")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("userAccountId").AsInt32().NotNullable().Indexed()
            .WithColumn("blockchainGateType").AsString(100).NotNullable()
            .WithColumn("publicKey").AsString(1000)
            .WithColumn("share").AsString(5000);

        Create.Table("gateconfig")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("blockchainGateType").AsString(100).NotNullable().Indexed()
            .WithColumn("config").AsString(1000);
        
        Create.Table("fanzeeevent")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("eventType").AsString(100).NotNullable()
            .WithColumn("data").AsString(1000)
            .WithColumn("ticks").AsInt64().NotNullable();
        Create.UniqueConstraint().OnTable("fanzeeevent").Columns("eventType", "ticks");

        Create.Table("ordercompletedqueuebinding")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("customerOrderId").AsInt32().NotNullable().Indexed()
            .WithColumn("routingKey").AsString(100).NotNullable()
            .WithColumn("queueName").AsString(100).NotNullable()
            .WithColumn("expiresAt").AsDateTimeOffset().NotNullable();
        
        Create.Table("challengetaskconfig")
            .WithColumn("id").AsInt32().Identity().PrimaryKey()
            .WithColumn("challengeId").AsInt32().NotNullable().Unique()
            .WithColumn("expiresAt").AsString().NotNullable();
    }

    public override void Down() {
        Delete.Table("challenge");
        Delete.Table("useraccount");
        Delete.Table("customer");
        Delete.Table("actionstate");
        Delete.Table("customeractionstate");
        Delete.Table("publisher");
        Delete.Table("collection");
        Delete.Table("tokenitem");
        Delete.Table("tokenitemtype");
        Delete.Table("category");
        Delete.Table("tokenitemtypecategory");
        Delete.Table("package");
        Delete.Table("refreshtoken");
        Delete.Table("userrole");
        Delete.Table("customerpackage");
        Delete.Table("packagecontent");
        Delete.Table("droppedhistory");
        Delete.Table("customerfollowing");
        Delete.Table("otpverification");
        Delete.Table("customerchallenge");
        Delete.Table("challengecontent");
        Delete.Table("customerorder");
        Delete.Table("customerorderitem");
        Delete.Table("payment");
        Delete.Table("useraccountwallet");
        Delete.Table("gateconfig");
        Delete.Table("fanzeeevent");
        Delete.Table("ordercompletedqueuebinding");
        Delete.Table("challengetaskconfig");
    }
}